from django.urls import path
from .views import friend_list, index

urlpatterns = [
    path('friends',friend_list, name='friend_list'),
    path('',index, name='index'),
    # TODO Add friends path using friend_list Views
]
