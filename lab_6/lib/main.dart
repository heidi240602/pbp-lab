import 'package:flutter/material.dart';

void main(){
  runApp(MyApp());
}

class MyApp extends StatefulWidget{
  @override
  _MyAppState createState() => _MyAppState();
}
class _MyAppState extends State<MyApp>{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home:Scaffold(
        appBar: AppBar(
          leading:Icon(IconData(0xef88, fontFamily: 'MaterialIcons'),color:Colors.blueGrey),
          backgroundColor: Colors.lightGreen,
          title:Text("Teman Covid",style:TextStyle(color:Colors.black54,fontWeight:FontWeight.bold,fontSize: 24)),
          actions: [ElevatedButton(onPressed: (){},
              child: Text('Login',style:TextStyle(color:Colors.white,fontWeight: FontWeight.normal)),
              style:ElevatedButton.styleFrom(primary:Colors.lightGreen),
          )]
        ),


        body:Center(
          child:Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                height:400.0,
                width:400.0,
                color:Colors.transparent,

                child:Container(
                  decoration: BoxDecoration(
                    color:Colors.green,
                    borderRadius:BorderRadius.all(Radius.circular(9.0)),),
                  child:Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children:<Widget>[
                      Text("Teman terbaik disaaat COVID melanda",style:TextStyle(fontSize: 20,fontWeight: FontWeight.normal),textAlign: TextAlign.center,),
                      Column(
                        children:<Widget>[
                          ElevatedButton(onPressed:(){},child:Text("Info Obat",style:TextStyle(color:Colors.black54)),style:ElevatedButton.styleFrom(primary:Colors.greenAccent),),
                          SizedBox(height: 10),
                          ElevatedButton(onPressed:(){}, child: Text("Sharing Pengalaman",style:TextStyle(color:Colors.black54)),style:ElevatedButton.styleFrom(primary:Colors.greenAccent)),
                          SizedBox(height: 10),
                          ElevatedButton(onPressed:(){}, child:Text("Wash Your Lyric",style:TextStyle(color:Colors.black54)),style:ElevatedButton.styleFrom(primary:Colors.greenAccent))
                        ]
                      )
                    ]
                  ),
              ),
              )],
          )
        ),
      ),
    );
  }
}