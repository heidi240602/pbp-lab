from lab_1.models import Friend
from django.forms import ModelForm
from django import forms

class DateInput(forms.DateInput):
    input_type="date"
class FriendForm(ModelForm):
    class Meta:
        model=Friend
        fields=['name', 'npm',
        'DOB']
        widgets={'DOB':DateInput()}


# Create your models here.