import 'package:flutter/material.dart';

void main(){
  runApp(ObatForm());
}

class ObatForm extends StatefulWidget {
  @override
  _ObatFormState createState() => _ObatFormState();
}

class _ObatFormState extends State<ObatForm> {
  TextEditingController _nameController =TextEditingController();
  TextEditingController _infoController =TextEditingController();

  Widget build(BuildContext context) {
    return MaterialApp(
      home:Scaffold(
        appBar:AppBar(),
        body:SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
            children:<Widget>[
              SizedBox(height:110),
              Container(
                alignment: Alignment.center,
                child:Text("Masukan Informasi Obat yang Ingin Ditambahkan",style:TextStyle(fontSize: 20,
                    fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
              ),
              SizedBox(height:20),
              Container(
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Container(
                      child:Text("Nama Obat"),
                    ),

                    Container(
                      constraints: BoxConstraints(minWidth: 100, maxWidth: 200,minHeight: 40,maxHeight: 40),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(const Radius.circular(10)),
                          border: Border.all(color: Colors.blueGrey, width: 1),
                    ),
                      child:TextField(
                        textAlign: TextAlign.center,
                        controller: _nameController,
                        decoration:const InputDecoration(
                          hintText: "Name"
                        ),
                      ),
                    ),
                    SizedBox(height:20),
                    Container(
                      child:Text("Info Obat"),
                    ),

                    Container(
                      constraints: BoxConstraints(minWidth: 100, maxWidth: 200,minHeight: 100,maxHeight: 100),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(const Radius.circular(10)),
                          border: Border.all(color: Colors.blueGrey, width: 1)
                      ),
                      child:TextField(
                        keyboardType: TextInputType.multiline,
                        minLines:1,
                        maxLines: 5,
                        textAlign: TextAlign.center,
                        controller: _infoController,
                        decoration:const InputDecoration(
                          hintText:"Your Info Here"
                        ),
                      ),
                    ),

                    Container(
                        child:ElevatedButton(
                          onPressed:(){
                            if(_nameController.text.isNotEmpty && _infoController.text.isNotEmpty){
                              final http.Response response= await http.post(Uri.parse('https://temancovid.herokuapp.com/informasi-obat/post'),
                                  body:{
                                    "name":_nameController.text,
                                    "info":_infoController.text,
                                  });
                              setState(() {
                                _nameController.text="";
                                _infoController.text="";
                              });
                            }
                          }, child:Text("Submit"),
                        )
                    ),
                  ],
                )
              )
            ]
          ),
        ),
      )
    );
  }
}

