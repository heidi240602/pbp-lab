from lab_2.models import Note
from django.forms import ModelForm
from django import forms

class NoteForm(ModelForm):
    class Meta:
        model=Note
        fields=['to','fromm','title','message']