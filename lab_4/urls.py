from django.urls import path
from .views import index,add_note,note_list, tutorial

urlpatterns = [
    path('',index, name='index'),
    path('tutorial',tutorial,name='tutorial'),
    # TODO Add friends path using friend_list Views
    path('add',add_note,name='add'),
    path('note-list',note_list,name='list')
]
