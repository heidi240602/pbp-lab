from django.shortcuts import render
from .forms import NoteForm
from lab_2.models import Note
from django.http import HttpResponse, HttpResponseRedirect
from django.core import serializers



def index(request):
    notes=Note.objects.all()
    response={"notes":notes}
    return render(request, 'lab4_index.html', response)
def note_list(request):
    notes=Note.objects.all()
    response={"notes":notes}
    return render(request,'lab_4_note_list.html',response)

def tutorial(request):
    return render(request,'tutorial.html')

def add_note(request):
    if request.method == 'POST':
        form =  NoteForm(request.POST)
        if form.is_valid():
            form.save()  # Save data to DB
            return HttpResponseRedirect('/lab-4')  # Redirect on finish

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NoteForm()

    return render(request, 'lab4_form.html', {'form': form})

# Create your views here.
